<?php

require_once "config.php";
require_once "lib/efergy.php";

$efergy = new Efergy(USERNAME, PASSWORD);
$efergy->connect();
if ($efergy->isConnected()) {
    $data = $efergy->getLast24Hours();
    if ($data->status == 'ok') {
        $tz = new DateTimeZone(TIMEZONE);
        foreach($data->data as $time => $reading) {
            $datetime = new DateTime('@' . $time / 1000, $tz);
            $watts = 1000 * $reading[0];
            echo sprintf("[%s] %dW\n", $datetime->format('Y-m-d H:i:s'), $watts);
        }
    }
}

