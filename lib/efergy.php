<?php

class Efergy {

    const BASE_URL = "http://engage.efergy.com";
    const BASE_API_URL = "http://engage.efergy.com/proxy";
    const USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.3 (KHTML, like Gecko) Chrome/22.0.1221.1 Safari/537.3";

    private $username;
    private $password;
    private $cookies = array();

    /**
     * Constructor method
     *
     * @param string $username
     * @param string $password
     * @access public
     * @return Efergy object
     */
    public function __construct($username, $password) {
        $this->username = $username;
        $this->password = $password;
        $this->cookies = NULL;
    }

    public function connect() {
        $this->getCookie();
        return $this->isConnected();
    }

    public function isConnected() {
        return ($this->cookies !== NULL);
    }

    public function getInstantReading() {
        $url = self::BASE_API_URL . '/getInstant';
        $data = $this->getData($url);
        return $data;
    }

    public function getLast24Hours() {
        $url = self::BASE_API_URL . '/getDay?getPreviousPeriod=0&cache=false&offset=-120';
        $data = $this->getData($url);
        return $data;
    }

    private function getCookie() {

        $url = self::BASE_URL . '/user/login';
        $encoded = 'username=' . urlencode($this->username) . '&password=' . urlencode($this->password) . '&remember=on';

        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_USERAGENT, self::USER_AGENT);
        curl_setopt($handle, CURLOPT_POST, 1);
        curl_setopt($handle, CURLOPT_POSTFIELDS,  $encoded);
        curl_setopt($handle, CURLOPT_HEADER, 1);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_TIMEOUT, 10);
        $output = curl_exec($handle);
        $http_status = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        curl_close($handle);

        if ($http_status != 302) {
            throw new InvalidCredentials();
        }

        $matches = array();
        $result = preg_match_all('/Set-Cookie: (session_name=([^;]*));/', $output, $matches);
        if ($result) {
            $this->cookies[] = $matches[1][count($matches[1])-1];
        }

        $matches = array();
        $result = preg_match('/Set-Cookie: (authautologin=([^;]*));/', $output, $matches);
        if ($result) {
            $this->cookies[] = $matches[1];
        }

    }

    private function getData($url) {

        if (!$this->isConnected()) {
            throw new NotConnected();
        }

        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_USERAGENT, self::USER_AGENT);
        curl_setopt($handle, CURLOPT_COOKIE, implode(';', $this->cookies));
        curl_setopt($handle, CURLOPT_POST, 0);
        curl_setopt($handle, CURLOPT_HEADER, 0);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_TIMEOUT, 10);
        $output = curl_exec($handle);
        curl_close($handle);
        return json_decode($output);
    }

}

class InvalidCredentials extends Exception {}
class NotConnected extends Exception {}

